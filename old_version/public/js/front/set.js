/*
$('div.dropdown-menu').on('click', function(event) {
    event.stopPropagation();
});
$(".close-drop").click(function() {
    $('.open').removeClass('open');
});
*/


window.onload = function(){

    //Check File API support
    if(window.File && window.FileList && window.FileReader)
    {
        var filesInput = document.getElementById("choose-images");
        
        filesInput.addEventListener("change", function(event){
            
            var files = event.target.files; //FileList object
            var output = document.getElementById("result-images");
            
            for(var i = 0; i< files.length; i++)
            {
                var file = files[i];
                
                //Only pics
                if(!file.type.match('image'))
                  continue;
                
                var picReader = new FileReader();
                
                picReader.addEventListener("load",function(event){
                    
                    var picFile = event.target;
                    
                    var div = document.createElement("div");
                    
                    div.innerHTML = "<img src='" + picFile.result + "'" +
                            "title='" + picFile.name + "'/><div class='bg-hover'></div><div class='delete-image'></div>";
                    
                    output.insertBefore(div,null);            
                
                });
                
                 //Read the image
                picReader.readAsDataURL(file);
            }                               
           
        });
    }
    else
    {
        console.log("Your browser does not support File API");
    }

}

$("#processing-task .close-message-task").click(function() {
    $('#processing-task').attr("style", "display: none");
});
$("#complite-task .close-message-task").click(function() {
    $('#complite-task').attr("style", "display: none");
});
$("#your_policer .close-message-task").click(function() {
    $('#your_policer').attr("style", "display: none");
});