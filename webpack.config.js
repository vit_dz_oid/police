'use strict';

const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');

const NODE_ENV = (process.env.NODE_ENV || 'production').trim();

let webpack_conf = {
    entry: {
        app: ['./app/initialize']
    },
    module: {
        loaders: [
            {
                test: /\.js?$/,
                exclude: /node_modules/,
                loader: 'babel',
                query: {
                    presets: ['es2015']
                }
            },
            {
                test: /\.html$/,
                loader: 'underscore-template-loader',
                query: {
                    parseDynamicRoutes: true
                }
            },
            {
                test: /\.css$/,
                include: /node_modules/,
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader')
            },{
                test: /\.css$/,
                exclude: /node_modules/,
                loader: "style-loader!css-loader"
            },{
                test: /.(png|jpg|jpeg)$/,
                loader: 'file?name=assets/img/[name]-[hash].[ext]'
            },{
                test: /.(ttf|eot|woff|woff2|svg)$/,
                loader: 'file?name=assets/fonts/[name]-[hash].[ext]'
            }
        ]
    },
    output: {
        filename: 'app.js',
        path: path.join(__dirname, './public'),
        publicPath: '/'
    },
    plugins: [
        new ExtractTextPlugin('app.css'),
        new CopyWebpackPlugin([
            {
                from: './app/assets/index.html',
                to: './index.html'
            },
            {
                from: './app/assets/img/pin',
                to: './assets/img/pin'
            },
            {
                from: './app/assets/img/share',
                to: './assets/img/share'
            },
            {
                from: './app/assets/img/favicon',
                to: './assets/img'
            },
            {
                from: './app/assets/vendor',
                to: './vendor'
            }
        ],{
            ignore: [
                './app/assets/img/front'
            ]
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: "jquery",
            _: 'underscore'
        })
    ],
    resolve: {
        root: path.join(__dirname, './app')
    },
    resolveLoader: {
        root: path.join(__dirname, './node_modules')
    },
    devtool: NODE_ENV === 'production' ? null: 'source-map',

    devServer : {
        host: 'localhost',
        port: 8080,
        historyApiFallback: true,
        contentBase: 'public/',
        proxy: [{
            path : '/api',
            target: 'http://police-api.thedigitalcrafters.com',
            secure: false,
            changeOrigin: true,
            bypass: function(req, res, proxyOptions) {
                console.log("Request ", req.originalUrl, req.method, req.body);
            }
        }]
    }
};

if(NODE_ENV === 'production'){
    webpack_conf.plugins.push(
        new webpack.optimize.UglifyJsPlugin()
    )
}

module.exports = webpack_conf;
