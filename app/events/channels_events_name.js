/**
 * Created by DzEN on 11/3/2016.
 */

export const CHANNELS = {
    MAP_VIEW_ADD_POINT : 'map_view_add_point',
    MAP_VIEW : 'map_view'
};

export const EVENTS = {
    POINT_ADD : 'point:add',
    MAP_INITIALIZED : 'map:initialized',
    MAP_GET : 'map:get',
    MAP_GET_DISTRICTS : 'map:get_districts',
    MAP_GET_CURRENT_CITY : 'map:get_current_city',
    ADD_FORM_SHOW : 'add_from:show',
    DETAILED_SHOW : 'detailed:show',
    TROUBLE_PLACE_COP_DETAILED_SHOW : 'trouble_place_cop:detailed:show',
    DETAILED_CLOSE : 'close_detailed_view',
    FORM_SUBMITTED : 'form:submitted'
}