import Backbone from 'backbone';
import PolicemanModel from '../models/PolicemanModel';
import _ from 'underscore';
import Storage from '../fake/FakeServer';
import log from 'loglevel';
import config from '../config';

export default Backbone.Collection.extend({
    model: PolicemanModel,
  initialize: function (array, options) {
    var self = this;

    self.city = options.city;
  },
    url: function(){
      let self = this;
      return config.ROOT_API_URL + "policemans?city=" + (self.city || 'kyiv') ;
    },
  parse: function(resp, opt) {
    console.log('Policemen : parse | ', resp);
    // Go use f*]cking ES6, yeeee!
    const newResp = resp.map((item) => {
      const data = {
        contact: {
          address: item.address,
          tel: item.phone
        },
        work_time: {
          days: item.schedule,
          time: null
        },
        position: {
          lat: parseFloat(item.lat),
          lng: parseFloat(item.lng)
        },
        photo: item.avatar
      };

      data.region =  _.map(item.region, function(item){
        return {
          lat: parseFloat(item.lat),
          lng: parseFloat(item.lng)
        }
      });
      log.info('ready element : ', _.extend(item, data));
      return _.extend(item, data);
    });

    return Backbone.Model.prototype.parse.apply(this, newResp, opt);
  },
});