/**
 * Created by DzEN on 9/14/2016.
 */
import Backbone from 'backbone';
import TroublePlaceModel from '../models/TroublePlaceModel';
import config from '../config';
import Storage from '../fake/FakeServer';
import log from 'loglevel';

export default Backbone.Collection.extend({
    model: TroublePlaceModel,
    initialize: function (array, options) {
        var self = this;

        self.city = options.city;
    },
    url: function(){
        let self = this;
        return config.ROOT_API_URL + "appeals?city=" + (self.city || 'kyiv') ;
    }
});
