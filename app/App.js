/**
 * Created by DzEN on 9/10/2016.
 */
import Bb from 'backbone';
import Marionette from 'backbone.marionette';
import $ from 'jquery';
import jQuery from 'jquery';
// export for others scripts to use
//import ItemView from 'components/main_page/FullView';
import Router from 'Routing';

window.$ = $;
window.jQuery = jQuery;

export default Marionette.Application.extend({
    region: 'body',

    onStart: function(){
        let router = new Router({ region : this.getRegion() });
        Bb.history.start();
        //Bb.history.start({pushState: true})
    }
});
