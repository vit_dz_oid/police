/**
 * Created by DzEN on 9/14/2016.
 */
import Bb from 'backbone';
import Mn from 'backbone.marionette';
import _ from 'underscore';
import MainPage from '../components/main_page/FullView';
import IndexPage from '../components/index_page/IndexView';
import AboutPage from '../components/about_page/AboutView';
import MobilePage from '../components/mobile/MobilePage';
import log from 'loglevel';

export default Mn.Object.extend({
    show_page: function(page){
        this.getOption('region').show(page);
    },
    index_page: function () {
        log.info("Show index page! ", this.getOption('region'));
        this.show_page(new IndexPage());
    },
    main_page: function () {
        log.info("Show main page! Region : ", this.getOption('region'));
        this.show_page(new MainPage());
    },
    about_page: function(){
        log.info("Show about page! Region : ", this.getOption('region'));
        this.show_page(new AboutPage());
    },
    mobile_page: function() {
      log.info("Show mobile page! Region : ", this.getOption('region'));
      this.show_page(new MobilePage());
    }
});