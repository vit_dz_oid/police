/**
 * Created by DzEN on 9/14/2016.
 */
import loadGoogleMapsAPI from 'load-google-maps-api';
import log from 'loglevel';
import consfig from '../config';

let promiseMapLoader = null;

export default function() {
  // consolelog('Google object download | ', !promiseMapLoader);
  if(!promiseMapLoader) {
    promiseMapLoader = new Promise((resolve, error)=> {
      if (document.readyState == 'loading') {
        document.addEventListener('DOMContentLoaded', () => {
          // consolelog("GoogleMap load with DOMContentLoaded");
          resolve();
        });
      } else {
        // consolelog("GoogleMap loaded");
        resolve();
      }

    })
      .then(() => {
      //Get google map constructor
      var a = loadGoogleMapsAPI({
        key: consfig.GOOGLE_API_KEY,
        libraries: ['places', 'geometry']
      }).then((googleMaps) => {
          log.info("Google map ", googleMaps); //=> Object { Animation: Object, ...
          return googleMaps;
        })
        .catch((err) => {
          log.error(err);
        });
      return a;
    });
  }

  return promiseMapLoader;
}
