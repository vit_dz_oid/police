/**
 * Created by DzEN on 9/14/2016.
 */
import GoogleObject from './GoogleObjectDownloader';
import log from 'loglevel';

export default function () {
  // consolelog('GoogleMap call!');
    return GoogleObject().then(map => {
      // consolelog('GoogleMap getted map!');
      let zoomLvl = 12;
      //Create and init map
      return new Promise((resolve, error) => {
        let mapProp = {
          //by default - show Kiev
          center: {
            lat: 50.444284,
            lng: 30.517609
          },
          zoom: zoomLvl,
          mapTypeId: map.MapTypeId.ROADMAP,
          zoomControl: true,
          zoomControlOptions : {
            position: map.ControlPosition.LEFT_BOTTOM
          },
          mapTypeControl: false,
          scaleControl: true,
          streetViewControl: false,
          rotateControl: false
        };
        resolve({
          map_prop_default : mapProp,
          google_maps  : map
        });
        //TODO resolve with https

          /*            //Getting user coordinates
           navigator.geolocation.getCurrentPosition(
           position => {
           log.info("Position ");
           let mapProp = {
           //TODO uncomment before deploy
           /!*center: {
           lat: position.coords.latitude,
           lng: position.coords.longitude
           },*!/
           center: {
           lat: 50.444284,
           lng: 30.517609
           },
           zoom: zoomLvl,
           mapTypeId: map.MapTypeId.ROADMAP,
           disableDefaultUI: true
           };
           resolve({
           map_prop_default : mapProp,
           google_maps  : map
           });
           },
           () =>{
           log.error("Map not initialized");
           error("Map not initialized");
           }
           )*/
      })
    });
}