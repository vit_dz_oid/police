/**
 * Created by DzEN on 9/15/2016.
 */
import Marionette from 'backbone.marionette';
import Radio from 'backbone.radio';
import $ from 'jquery';
import log from 'loglevel';

// import Gmap from '../../modules/GoogleMap';
import Toast from '../toast/Toast';
import template from './form.html';
// import Server from '../../fake/FakeServer';
import { CHANNELS, EVENTS } from '../../events/channels_events_name';
import ImagesPreview from '../image_prev/ImagesPreview';
import config from '../../config';

var need_validation_els = [
    {
        name : "address",
        pattern : /.{10,}/i
    },
    {
        name : "title",
        pattern : /.{5,}/i
    },
    {
        name : "message",
        pattern : /.{1,2048}/i
    },
    {
        name : "name",
        pattern : /.{3,}/i
    },
    {
        name : "email",
        pattern : /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i

    }
];

export default Marionette.View.extend({
    template: template,
    regions: {
        'images' : '#images-container'
    },
    events: {
        'submit form': 'submit_add_point',
        'keypress input': function(e) {
            return e.which !== 13;
        },
        'change #photos': 'image_checked',
        'blur input[type!="file"]': 'validate_input',
        'blur textarea': 'validate_input'
    },
    ui : {
        errorPlace: "",
        inputAddress :'#address',
        photos : '#photos'
    },
    initialize: function(){
        var self = this;
        //TODO : remove!
        //window.my_$ = $;

        log.info("AddPointForm is initialize");

        self.channel = Radio.channel(CHANNELS.MAP_VIEW_ADD_POINT);
        self.channel_map = Radio.channel(CHANNELS.MAP_VIEW);
        ({gmap: self.gmap, google_maps : self.google_maps} = self.channel.request(EVENTS.MAP_GET));

        self.images = [];
    },
    onRender : function(){
        var self = this;
        self.activate_input_find_place();
        self.showChildView('images', new ImagesPreview());
    },
    activate_input_find_place: function () {
        var self = this;
        log.info("Self ", self);
        var input = self.getUI('inputAddress')[0];

        var map = self.gmap;

        // Create the autocomplete helper, and associate it with
        // an HTML text input box.
        var autocomplete = new self.google_maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);
        var image = {
            url: 'assets/img/pin/pin_01.png',
            // This marker is 20 pixels wide by 32 pixels high.
            size: new self.google_maps.Size(50, 60),
            // The origin for this image is (0, 0).
            origin: new self.google_maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at (0, 32).
            anchor: new self.google_maps.Point(19, 45),
            scaledSize: new self.google_maps.Size(37.5, 45)
        };

        self.mapMarker = new self.google_maps.Marker({
            map: map,
            icon: image
        });

        // Get the full place details when the user selects a place from the
        // list of suggestions.

        self.map_listener = self.google_maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                return;
            }

            map.setCenter(place.geometry.location);
            map.setZoom(17);
            self.mapMarker.setOptions({url: place.url});


            // Set the position of the marker using the place ID and location.
            self.mapMarker.setPlace(/** @type {!self.google.Place} */ ({
                placeId: place.place_id,
                location: place.geometry.location
            }));
            self.mapMarker.setVisible(true);
        });
    },
    image_checked: function(e){
        var self = this;

        log.info("Event ", e);
        var files = e.target.files;
        self.images = [];
        // Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {

            // Only process image files.
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function(theFile) {
                return function(e) {
                    self.images.push({ result : e.target.result, title : escape(theFile.name), file: theFile});
                    self.getChildView('images').collection.reset(self.images);
/*                    // Render thumbnail.
                    var span = document.createElement('span');
                    span.innerHTML =
                        [
                            '<img style="height: 75px; border: 1px solid #000; margin: 5px" src="',
                            e.target.result,
                            '" title="', escape(theFile.name),
                            '"/>'
                        ].join('');

                    document.getElementById('result-images').insertBefore(span, null);*/
                };
            })(f);

            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
    },
    reset_form: function(){
        var self = this;

        //Clear input field and remove red border
        _.each(self.$('input'), function (el) {
            $(el).val('').removeClass('error-input');
        });

        _.each(self.$('textarea'), function (el) {
            $(el).val('').removeClass('error-input');
        });

        //Hide error message
        _.each(self.$('.error-message'), function (item) {
            $(item).addClass('hidden');
        });

        //Clear images
        //self.$('#result-images').empty();
        var control = self.$('#photos');
        control.replaceWith(control.val('').clone(true));

        self.getChildView('images').delete_all_images();

        !!self.mapMarker ? self.mapMarker.setVisible(false) : null;
    },
    submit_add_point: function (e) {
        var self = this;

        e.preventDefault();

        if (self.new_validation()) {
            log.info("Validation is pass!");

            var formdata = self.$('form').serializeArray();
            log.info("Form is valid ", formdata, self.mapMarker);
            var data = {};

            self.disable_form();

            _.each(formdata, function(item){
                data[item.name] = item.value;
            });

            var formData = new FormData($("#request_form")[0]);
            const city = self.channel_map.request(EVENTS.MAP_GET_CURRENT_CITY);
            /*  "district_id" => "1",
             "full_address" => "Пушкіна Тра Та та",
             "city" => "Віннікі",
             "subject" => "Тема",
             "message" => "Текст тра трата тртв атвратрв",
             "name" => "Вася",
             "email" => "example@dsf.com",
             "lat" => "50,1",
             "lng" => "23,2",
             "photos" =*/
            formData.set("district_id", 0);
            formData.set("city_id", city.id || -1);
            formData.set("full_address", data.address);
            formData.set("subject", data.title);
            formData.set("message", data.message);
            formData.set("city", city.name || '');
            formData.set("email", data.email || '');
            formData.set("lat", self.mapMarker.place.location.lat());
            formData.set("lng", self.mapMarker.place.location.lng());
            let photos_el = self.$el.find('#photos')[0];
            for(let i = 0 ; i < photos_el.files.length; i++){
                formData.append("photos[]", photos_el.files[i], photos_el.files[i].name);
            }
            let photos_real = self.getChildView('images').collection.pluck('file');
            for(let i = 0 ; i < photos_real.length; i++){
                formData.append("photos_real[]",photos_real[i], photos_real[i].name);
            }

            $.ajax({
                url: config.ROOT_API_URL + "appeals",
                type: 'POST',
                contentType: false,
                processData: false,
                data: formData,
                async: true,
                success: function (data) {
                    new Toast({message : "Запит успішно відправлений"});
                    self.trigger(EVENTS.FORM_SUBMITTED);
                },
                error: function(jqXHR, textStatus, errorThrown){
                    log.warn("Add point error : ", arguments);
                    new Toast({message : "Ми не змогли відправити дані!" }, {error : true});
                },
                statusCode : {
                  413: function() {
                    log.warn("IMG ERROR : ", arguments);

                    new Toast({message : "Зображення мають завеликий об'єм" }, {error : true});
                  }
                },
                complete: function(){
                    self.enable_form();
                },
                cache: false
            });
        }
    },
    disable_form: function(){
        var self = this;
        self.form_toggle(false);
    },
    enable_form: function(){
        var self = this;
        self.form_toggle(true);
    },
    form_toggle: function(flag){
        var self = this;

        _.each(self.$('input'), function(el){
            flag ? $(el).removeAttr("disabled") : $(el).attr("disabled", "disabled");
        });

        _.each(self.$('textarea'), function(el){
            flag ? $(el).removeAttr("disabled") : $(el).attr("disabled", "disabled");
        });

        _.each(self.$('button'), function(el){
            flag ? $(el).removeAttr("disabled") : $(el).attr("disabled", "disabled");
        });
      debugger;
        this.triggerMethod('toggle:form', flag);
    },
    new_validation : function(){
        var self = this;
        var hasError = true;
        var form = self.$el;
        var el = null;

        _.each(need_validation_els, function(item){
            el = form.find("[name='" + item.name + "']");

            if (!item.pattern.test(el.val())){
                el.addClass('error-input');
                el.find('+ .error-message').removeClass('hidden');
                //Toggle that have as min one error field
                hasError = false;
            } else {
                el.removeClass('error-input');
                el.find('+ .error-message').addClass('hidden');
            }
        });

        return hasError;
    },
    validate_input : function(e){
        var self = this;
        var value = e.target.value;
        var $el = $(e.target);
        var pattern = _.chain(need_validation_els).findWhere({name: $el.attr('name')}).value();

        log.info("Pattern ", pattern);

        if(pattern){
           pattern = pattern.pattern;
        } else {
            return;
        }

        if (!pattern.test(value)){
            $el.addClass('error-input');
            $el.find('+ .error-message').removeClass('hidden');
        } else {
            $el.removeClass('error-input');
            $el.find('+ .error-message').addClass('hidden');
        }
    },
  //REmove districts
    /*get_district_of_point: function(){
        let self = this;

        let districts = self.channel.request(EVENTS.MAP_GET_DISTRICTS).districts;
        let marker_point = self.mapMarker.place.location;

        //Find district when trouble occurs
        let district = _.find(districts, function (item) {
            let coords = _.map(item.coords, function(item){
                return {
                    lat: parseFloat(item.lat),
                    lng: parseFloat(item.lng)
                }
            });
            let b = new self.google_maps.Polygon({paths: coords});
            log.info("Data for search cop ", marker_point,b);
            if (self.google_maps.geometry.poly.containsLocation(marker_point, b)) {
                return true;
            }
        });

        return district;
    }*/
});

