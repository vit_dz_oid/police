/**
 * Created by DzEN on 9/15/2016.
 */
import Marionette from 'backbone.marionette';
import Radio from 'backbone.radio';
import log from 'loglevel';

import Form from './AddPointForm';
import template from './popup_bar.html';
import { CHANNELS, EVENTS } from '../../events/channels_events_name';

export default Marionette.View.extend({
    template: template,
    tagName: 'form',
    regions: {
        content: '.content'
    },
    events: {
        'click .add-form-modal .dropdown-toggle, .add-form-modal .close-drop': 'show_add_point_modal'
    },
    ui: {
        window: '.add-form-modal'
    },
    onChildviewToggleForm: function(val) {
        debugger;
        const methodName = val ? 'removeClass' : 'addClass';
        this.$el[methodName]('disable');
    },
    windowBar: null,
    initialize:function () {
        var self = this;

        log.info('AddPointWindow is initialize ');
        self.channel = Radio.channel(CHANNELS.MAP_VIEW_ADD_POINT);
        self.listenTo(self.channel, EVENTS.DETAILED_SHOW, () => {
            if (self.windowBar) {
                self.prepare_form();
                self.windowBar.removeClass('open');
            }
        });
    },
    show_add_point_modal: function () {
        var self = this;

        if (!self.windowBar) {
            self.windowBar = self.getUI('window');
        }

        if (!self.windowBar.hasClass('open')) {
            self.channel.trigger(EVENTS.ADD_FORM_SHOW);
        }

        self.prepare_form();

        self.windowBar.toggleClass('open');
    },
    prepare_form: function () {
        var self = this;

        if (!self.form) {
            self.form = new Form();
            self.listenTo(self.form, EVENTS.FORM_SUBMITTED, self.show_add_point_modal);
            self.showChildView('content', self.form);
        }

        self.form.reset_form();
    }
});