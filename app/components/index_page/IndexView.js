/**
 * Created by DzEN on 9/19/2016.
 */
import Backbone from 'backbone';
import Marionette from 'backbone.marionette';

import $ from 'jquery';
import log from 'loglevel';

import template from './index.html';
import config from '../../config';

var need_validation_els = [
    {
        name : "name",
        pattern : /.{3,}/i
    },
    {
        name : "email",
        pattern : /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i
    },
    {
        name : "message",
        pattern : /.{10,80}/i
    }
]

export default Marionette.View.extend({
    template: template,
    events: {
        'click a.city': 'city_checked',
        'click .btn-add-city': 'add_city_from_check_city_modal',
        /*'click a.add-own-city' : 'restore_form',*/
        'click #submit-form-add-city': 'submit_add_city',
        'blur input[type!="file"]': 'validate_input',
        'blur textarea': 'validate_input',
        'click .wrap_a_s_p': 'stop_close_modal',
        'click .add-own-city-bottom': 'cause_add_own_city_click'
    },
    regions: {
    },
    onRender: function(){
        var self = this;

        self.$('a.add-own-city').on('click', self.restore_form.bind(self));

        setTimeout(function(){
            self.scroll_to_body_top()
        }, 0);
    },
    onDestroy: function(){
        var self = this;

        log.info("Destroy view index");
        self.$('a.add-own-city').off('click');
    },
    city_checked: function(e){
        var path = 'map/' + $(e.currentTarget).data('city');
        log.info("Navigate to ", path);

        setTimeout(function(){
            //TODO: remove default kiev
            Backbone.history.navigate(path, {trigger: true});
        }, 250);
    },
    add_city_from_check_city_modal: function(e){
        var self = this;

        log.info('This : ', self);

        e.stopPropagation();
        $('.add_sity_pop a.add-own-city').dropdown('toggle');
        $('#modal-ch-s').modal('hide');
        self.restore_form();
        self.toggle_form_add_city(true);
    },
    restore_form: function(){
        var self = this;
        //Clear input field and remove red border
        _.each(self.$('#form-add-city input'), function (el) {
            $(el).val('').removeClass('error-input');
        });

        _.each(self.$('#form-add-city textarea'), function (el) {
            $(el).val('').removeClass('error-input');
        });

        //Hide error message
        _.each(self.$('#form-add-city .error-message'), function (item) {
            $(item).addClass('hidden');
        });
        self.toggle_form_add_city(true);
    },
    submit_add_city: function(e){
        var self = this;
        e.stopPropagation();
        e.preventDefault();
        if(!self.new_validation()){
            return false;
        }
      var data = $(self.$('#form-add-city')).serializeArray();
      var dataObj = _.object(_.pluck(data, 'name'), _.pluck(data, 'value'));

      $.ajax({
        url: config.ROOT_API_URL + "city_proposals",
        type: 'POST',
        data: dataObj,
        success: function() {
          console.log('Request add city success');
        }
      });
        self.toggle_form_add_city(false);
    },
    toggle_form_add_city: function (val){
        var self = this;

        if(val){
            self.$el.find('.wrap_a_s_p').css('display', 'block');
            self.$el.find('.wrap_a_s_p_success').css('display', 'none');
        } else {
            self.$el.find('.wrap_a_s_p').css('display', 'none');
            self.$el.find('.wrap_a_s_p_success').css('display', 'block');
        }
    },
    validate_input : function(e){
        var self = this;
        var value = e.target.value;
        var $el = $(e.target);
        var pattern = _.chain(need_validation_els).findWhere({name: $el.attr('name')}).value();

        log.info("Pattern ", pattern);

        if(pattern){
            pattern = pattern.pattern;
        } else {
            return;
        }

        if (!pattern.test(value)){
            $el.addClass('error-input');
            $el.find('+ .error-message').removeClass('hidden');
        } else {
            $el.removeClass('error-input');
            $el.find('+ .error-message').addClass('hidden');
        }
    },
    new_validation : function(){
        var self = this;
        var hasError = true;
        var form = self.$el.find('#form-add-city');
        var el = null;

        _.each(need_validation_els, function(item){
            el = form.find("[name='" + item.name + "']");

            if (!item.pattern.test(el.val())){
                el.addClass('error-input');
                el.find('+ .error-message').removeClass('hidden');
                //Toggle that have as min one error field
                hasError = false;
            } else {
                el.removeClass('error-input');
                el.find('+ .error-message').addClass('hidden');
            }
        });

        return hasError;
    },
    scroll_to_body_top : function(){
        $('html,body').animate({scrollTop: $('body').offset().top},'slow')
    },
    stop_close_modal: function(e){
        e.stopPropagation();
        e.preventDefault();
    },
    cause_add_own_city_click: function () {
        $('.add-own-city').get(0).click();
        return false;
    }
});