/**
 * Created by DzEN on 11/9/2016.
 */
import Marionette from 'backbone.marionette';
import PolicemanView from './PolicemanView';

export default PolicemanView.extend({
    remove : function(){
        Marionette.View.prototype.remove.apply(this, arguments);
    }
});
