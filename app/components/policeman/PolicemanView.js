/**
 * Created by DzEN on 9/14/2016.
 */
import Marionette from 'backbone.marionette';
import PolicemanModel from '../../models/PolicemanModel';
import AddRequestToCopModal from '../modal/AddRequestToCop';
import template from './policeman.html';
import { EVENTS } from '../../events/channels_events_name';

export default Marionette.View.extend({
    tagName: 'div',
    className: 'message-task your_policer',
    template: template,
    events: {
        'click .close-message-task': 'close_message_task',
        'click .connect_us__toggle': 'show_request_to_cop_modal'
    },
    initialize: function(){
        this.model = new PolicemanModel(this.model);
    },
    close_message_task: function () {
        this.remove();
    },
    remove: function () {
        this.trigger(EVENTS.DETAILED_CLOSE);
        Marionette.View.prototype.remove.apply(this, arguments);
    },
    show_request_to_cop_modal : function(){
        var self = this;

        (new AddRequestToCopModal({'model' : self.model.toJSON()})).render();
    }
})