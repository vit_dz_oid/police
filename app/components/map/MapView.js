/**
 * Created by DzEN on 9/14/2016.
 */
import Marionette from 'backbone.marionette';
import Backbone from 'backbone';
import Radio from 'backbone.radio';
import _ from 'underscore';
import log from 'loglevel';
import MarkerClusterer from 'node-js-marker-clusterer';

import Gmap from '../../modules/GoogleMap';
import TroublePlaceModel from '../../models/TroublePlaceModel';
import Policemen from '../../collections/PolicemanCollection';
import TroublePlaces from '../../collections/TroublePlacesCollection';
import Districts from '../../collections/DistrictsCollection';
import PolicemanView from '../policeman/PolicemanView';
import PolicemanViewAsChildTroublePlace from '../policeman/PolicemanViewAsChildTroublePlace';
import TroublePlaceView from '../trouble_place/TroublePlaceView';
import Server from '../../fake/FakeServer';
import { CHANNELS, EVENTS } from '../../events/channels_events_name';
import Toast from '../toast/Toast';
import template from './map_block.html';

var staticCitiesCenter = {
     kyiv : {
        lat: 50.444284,
        lng: 30.517609
    },
    odesa : { //46.455936, 30.734447
        lat: 46.455936,
        lng: 30.734447
    },
    kherson : { //46.643953, 32.608042
        lat: 46.643953,
        lng: 32.608042
    },
    mykolaiv : { //46.960636, 32.016914
        lat: 46.960636,
        lng: 32.016914
    },
    vinnytsia : { // 49.232777, 28.469288
      lat: 49.232777,
      lng: 28.469288
    },
    'ivano-frankivsk' : { // 48.922438, 24.710301
      lat: 48.922438,
      lng: 24.710301
    },
    lviv : { // 49.843301, 24.026629
      lat: 49.843301,
      lng: 24.026629
    }
};

export default Marionette.View.extend({
    id: 'mapBlock',
    tagName: 'div',
    template: template,
    initialize: function (options) {
        let self = this;
        log.info('Map view loaded!');
        self.city = options.city || null;
        self.set_channels();
        Gmap().then(
            gm => {
                log.info("GM : ", gm);
                self.map_initializer(gm);
            },
            error => log.error("Map not created ", error))
            .then(() => {
                if(!self.city){
                    /*
                     * When user go to url 'map/' - we dont have city (in this case, example, url will be map/kiev).
                     * Getting user location:  if user in some city - search city in our DB - show result, else -
                     * show some else (at 08/11/16 - i don't know what it must be)
                     * */
                    return new Promise((resolve, error) => {
                        //Getting user coordinates
                        navigator.geolocation.getCurrentPosition(
                            position => {
                                let userCoordinates = {
                                    //TODO uncomment before deploy
                                    /*                            center: {
                                     lat: position.coords.latitude,
                                     lng: position.coords.longitude
                                     },*/
                                    center: {
                                        lat: 49.316704, //50.444284,
                                        lng: 28.327009 //30.517609
                                    }
                                };

                                resolve(userCoordinates);
                            },
                            () => {
                                log.error("Error in location city finding");
                                error("Error in location city finding");
                            }
                        )
                    }).then((userCoordinates) => {
                        return new Promise((resolve, error) => {
                            $.ajax({
                                type: 'GET',
                                dataType: "json",
                                url: "http://maps.googleapis.com/maps/api/geocode/json?latlng=" +
                                userCoordinates.center.lat + "," +
                                userCoordinates.center.lng + "&sensor=false",
                                success: function (data) {
                                    let hasCity = data['results'].some((res_element) => {
                                        let hasEl = res_element['address_components'].some((adr_element) => {
                                            if (adr_element['types'] == "locality,political") {
                                                if (adr_element['long_name'] != "") {
                                                    self.city = adr_element['long_name'].toLowerCase();
                                                    resolve();
                                                    return true;
                                                } else {
                                                    return false;
                                                }
                                            }
                                        });

                                        return !!hasEl;
                                    });
                                    //If city not exist we must complete finding and call resolve()
                                    !hasCity ? resolve() : null;
                                },
                                error: function () {
                                    log.error("Cant receive city for user");
                                    error("Cant receive city for user");
                                }
                            });
                        });
                    });
                }
            })
            .then(() => {
                log.info("City : ", self.city);
                self.markersTroublePlaces = new TroublePlaces([], {city: self.city});
                self.districts = new Districts([], {city: self.city});
                self.show_preloader(true);
                return Promise.all([
                    new Promise(function (resolve, error) {
                        self.markersTroublePlaces.fetch({
                            success: ()=> { log.info("Troubleplaces loaded!"); resolve()},
                            error: error
                        })
                    }),
                    new Promise(function (resolve, error) {
                        // TODO : disabled policeman
                        self.markersPolicemans = new Backbone.Collection();
                        resolve();
                        /* self.markersPolicemans = new Policemen([], {city: self.city});
                        self.markersPolicemans.fetch({
                            success : function (){
                                log.info("Policemans loaded! ", self.markersPolicemans.toJSON());

                                resolve();
                            },
                            error : error
                        });*/

                        // Removed. Cops getted by url
                        /*self.districts.fetch({
                            success: function () {
                                let cops = self.districts.parse_to_policeman();
                                if(cops.length < 1) {
                                    resolve();
                                    return;
                                }
                                _.each(cops, function (item, index, arr) {
                                    self.markersPolicemans.add(new PolicemenModel(item, {parse_from_district: true}));
                                    if (index + 1 === arr.length) {
                                        //log.info("Policemans loaded!");
                                        resolve();
                                    }
                                });
                            },
                            error: () => resolve()
                        });*/
                    })
                ]);
            })
            .then(() => {
                let hasCityReal = staticCitiesCenter[self.city];
                if(hasCityReal){
                    log.info("MapView | initialize | city - ", hasCityReal);
                    self.gmap.setCenter(hasCityReal);
                }
                //log.info("Перевірка наявності міста");
                if(!staticCitiesCenter[self.city]){
                    new Toast(
                        {message : "Схоже, населений пункт ще не підключено до системи. Звяжіться, будь ласка, з " +
                        "адміністратором"},
                        {error: true}
                    );
                }
            })
            .then(()=> self.markers = self.initialize_markers(self.markersTroublePlaces, self.markersPolicemans))
            .then(()=> self.show_markers_on_map())
            .then(()=> self.show_preloader(false));
    },
    map_initializer: function (gm) {
        var self = this;

        self.gmap = new gm.google_maps.Map(document.getElementById('googleMap'), gm.map_prop_default);
        self.google_maps = gm.google_maps;

        //Global event, triggered when map initialized
        self.channel_map.trigger(EVENTS.MAP_INITIALIZED, {gmap: self.gmap, google_maps: self.google_maps});

        self.gmap.addListener('zoom_changed', function () {
            let zoom = this.getZoom();
            self.changed_zoom_lvl(zoom);
        });
    },
    initialize_markers: function (troublePlaces, policemans) {
        let self = this;
        let markers = {
            markers_cluster: [],
            markers_simple: []
        };

        _.each(troublePlaces.models, function (item) {
            let ready_marker = self.create_marker(item.toJSON());

            self.add_marker_listeners(ready_marker);

            if (item.get('resolved') === 0) {
                markers.markers_cluster.push(ready_marker);
            } else {
                markers.markers_simple.push(ready_marker);
            }
        });
        _.each(policemans.models, function (item) {
            let ready_marker = self.create_marker(item.toJSON());

            self.add_marker_listeners(ready_marker);

            markers.markers_simple.push(ready_marker);
        });

        return markers;
    },
    show_markers_on_map: function () {
        var self = this;

        let options = {
            gridSize: 50,
            zoomOnClick: true,
            averageCenter: true,
            iconAnchor: [27, 64],
            styles: [
                {
                    textColor: 'white',
                    url: 'assets/img/pin/pin_01_empty.png',
                    height: 66,
                    width: 54,
                    textSize: 16
                }
            ]
        };

        log.info("Markers ", self.markers);

        self.markerCluster = new MarkerClusterer(self.gmap, self.markers.markers_cluster, options);

        _.each(self.markers.markers_simple, function (marker) {
            marker.setMap(self.gmap);
        });
    },
    set_channels: function () {
        var self = this;

        //For getting point coordinates for create trouble place with data from sender
        self.channel = Radio.channel(CHANNELS.MAP_VIEW_ADD_POINT);
        self.listenTo(self.channel, EVENTS.POINT_ADD, self.add_new_problem_place);
        //For requester that wont get initialized map object
        self.channel.reply(EVENTS.MAP_GET, ()=> {
            return {gmap: self.gmap, google_maps: self.google_maps};
        });
        self.listenTo(self.channel, EVENTS.ADD_FORM_SHOW, () => {
            if (self.detailedWindow) {
                self.detailedWindow.remove();
            }
        });
        //For get districts from other parts of application
        self.channel.reply(EVENTS.MAP_GET_DISTRICTS, ()=> {
            return {districts: self.districts.toJSON()};
        });

        self.channel_map = Radio.channel(CHANNELS.MAP_VIEW);
        self.channel_map.reply(EVENTS.MAP_GET_CURRENT_CITY, () => {
            const cityId = self.districts && self.districts.at(0) &&
              self.districts.at(0).toJSON().city_id;
            return {
                name : self.city,
                id : cityId
            }
        })
    },
    create_marker: function (modelObject) {
        let self = this;

        let image = {
            url: modelObject.icon,
            // This marker is 20 pixels wide by 32 pixels high.
            size: new google.maps.Size(50, 60),
            // The origin for this image is (0, 0).
            origin: new google.maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at (0, 32).
            anchor: new google.maps.Point(19, 45),
            scaledSize: new google.maps.Size(37.5, 45)
        };
        return new self.google_maps.Marker({
            position: modelObject.position,
            icon: image,
            json_model: modelObject
        });
    },
    add_marker_listeners: function (ready_marker) {
        let self = this;

        ready_marker.addListener('click', function () {
            //If it is this same marker - no action
            if (ready_marker.is_open)
                return;

            //Have opened window - to close
            if (self.detailedWindow) {
                self.detailedWindow.remove();
            }

            //Marking marker as "opened"
            ready_marker.is_open = true;
            ready_marker.json_model.type === 'policeman' ? self.show_window_policeman(this.json_model)
                : self.show_window_trouble_place(this.json_model);
        });

        ready_marker.addListener('mouseover', function () {
            self.icon_hover(ready_marker, true);
        });

        ready_marker.addListener('mouseout', function () {
            !ready_marker.is_open ? self.icon_hover(ready_marker, false) : null;
        });
    },
    show_window_trouble_place: function (modelJSON) {
        let self = this;
        let detailedWindowTroublePlace = new TroublePlaceView({model: modelJSON});
        let detailedChildPolicemanView = null;

        self.listenTo(detailedWindowTroublePlace, EVENTS.DETAILED_CLOSE, function(){
            if(!!detailedChildPolicemanView){
                detailedChildPolicemanView.remove();
            };
            self.detailed_view_closed(...arguments);
        });
        self.listenTo(detailedWindowTroublePlace, EVENTS.TROUBLE_PLACE_COP_DETAILED_SHOW, function (model){
            detailedChildPolicemanView = self.show_window_policeman_of_trouble_place(model);
        });
        //Render window on map layer
        self.render_window(detailedWindowTroublePlace);
        return detailedWindowTroublePlace;
    },
    show_window_policeman: function (modelJSON) {
        let self = this;
        let detailedWindowPoliceman = new PolicemanView({model: modelJSON});

        self.set_policeman_region(modelJSON, detailedWindowPoliceman);

        self.listenTo(detailedWindowPoliceman, EVENTS.DETAILED_CLOSE, self.detailed_view_closed);

        //Render window on map layer
        self.render_window(detailedWindowPoliceman);
        return detailedWindowPoliceman;
    },
    show_window_policeman_of_trouble_place: function (modelJSON) {
        let self = this;
        let detailedWindowPoliceman = new PolicemanViewAsChildTroublePlace({model: modelJSON});

        self.$el.append(detailedWindowPoliceman.render().el);
        return detailedWindowPoliceman;
    },
    set_policeman_region: function (modelJSON, detailedWindowPoliceman) {
        let self = this;

        let polyOptions = {
            path: modelJSON.region,
            strokeColor: "#106ED6",//"#0077b9",
            strokeOpacity: 1,
            strokeWeight: 1,
            fillColor: "#106ED6",//"#00a4ff",
            fillOpacity: 0.15
        };

        let it = new self.google_maps.Polygon(polyOptions);
        it.setMap(self.gmap);

        self.listenTo(detailedWindowPoliceman, EVENTS.DETAILED_CLOSE, function () {
            it.setMap(null);
        });
    },
    render_window: function (detailedWindow) {
        let self = this;
        log.info("detailedWindow ", detailedWindow);
        self.detailedWindow = detailedWindow;
        self.detailedWindowEl = detailedWindow.render().el;
        self.channel.trigger(EVENTS.DETAILED_SHOW);
        log.info("Rendered window ", self.detailedWindowEl, self.$el);
        self.$el.append(self.detailedWindowEl);
    },
    icon_hover: function (marker, trigger) {
        let icon = marker.getIcon();

        let options = {};

        //True - if marker under hover
        if (trigger) {
            options = {
                anchor: new google.maps.Point(25, 60),
                scaledSize: new google.maps.Size(50, 60)
            };
        } else {
            options = {
                anchor: new google.maps.Point(19, 45),
                scaledSize: new google.maps.Size(37.5, 45)
            };
        }
        icon = _.extend(icon, options);
        marker.setIcon(icon);
    },
    detailed_view_closed: function () {
        let self = this;

        _.each(self.markers.markers_cluster.concat(self.markers.markers_simple), function (item) {
            if (item.is_open) {
                item.is_open = false;
                self.detailedWindow = null;
                self.icon_hover(item, false);
            }
        });
    },
    add_new_problem_place: function (markerData) {
        let self = this;

        /*
         address:"вулиця Антоновича, 10, Київ, місто Київ, Україна"
         email:"description@gmail.com"
         message:"Description DescriptionDescriptionDescriptionDescriptionDescriptionDescriptionDescriptionDescription DescriptionDescriptionDescriptionDescriptionDescriptionDescriptionDescription  DescriptionDescriptionDescription"
         name:"Name"
         position: {
         lat:50.438364,
         lng:30.513070999999968
         },
         title:"Title"}
         url:"https://maps.google.com/?q=Antonovycha+St,+109,+Kyiv,+Ukraine&ftid=0x40d4cf18d980ef0b:0xdf8bd3475dcc4aaa"
         */

        let data = {
            position: markerData.position,
            address: {
                text: markerData.address,
                url: markerData.url
            },
            resolved: 0,//0 - no, 1- in process, 2 - yes
            request_message: {
                title: markerData.title,
                message: markerData.message
            },
            photo: [
                {
                    url: "http://politica-ua.com/wp-content/uploads/2014/01/dep-sluga-pozvoni-23-01-2014.jpg"
                },
                {
                    url: "http://upogau.org/netcat_files/1496_273.jpg"
                },
                {
                    url: "https://focus.ua/modules/thumb.php?u=../files/images/5511/5511777.jpg"
                },
                {
                    url: "https://focus.ua/modules/thumb.php?u=../files/images/5508/5508831.jpg"
                }
            ],
            response_message: {
                title: "",
                message: ""
            },
            police_man: markerData.police_man || 1
        };

        log.info("Data ", data);
        let newTroublePlace = new TroublePlaceModel(data);
        log.info("New trouble place ", newTroublePlace);
        let newMarker = self.create_marker(newTroublePlace.toJSON());

        self.add_marker_listeners(newMarker);
        self.markers.markers_cluster.push(newMarker);
        self.markerCluster.addMarker(newMarker);

        //Add to localStorage
        let tpStorageObject = newTroublePlace.toJSON();

        Server().upd_local_storage(tpStorageObject, 'troublePlaces');
    },
    changed_zoom_lvl: function (zoom) {
        let self = this;
        let visible = false;

        if (zoom < 11) {
            visible = false;
        } else {
            visible = true;
        }

        _.each(self.markers.markers_simple, function (marker) {
            marker.setVisible(visible);
        });
    },
    show_preloader: function (toggler) {
        var self = this;

        if (toggler) {
            self.$('#googleMap').css('opacity', '0');
            self.$('.location_indicator').css('opacity', '1');
        } else {
            self.$('#googleMap').css('opacity', '1');
            self.$('.location_indicator').css('opacity', '0');
            setTimeout(() => self.$('.location_indicator').css('display', 'none'), 1000);
        }
    }
});