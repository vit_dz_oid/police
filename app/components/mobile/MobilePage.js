/**
 * Created by Tanya on 29.05.2017.
 */
import Marionette from 'backbone.marionette';
import $ from 'jquery';
import log from 'loglevel';

import template from './mobile.html';

export default Marionette.View.extend({
  template: template,
  className: 'wrap-mob-notice'
});