/**
 * Created by DzEN on 9/14/2016.
 */
import Marionette from 'backbone.marionette';
import _ from 'underscore';
import log from 'loglevel';

import template from './troublePlace.html';
import PolicemanModel from '../../models/PolicemanModel';
import Server from '../../fake/FakeServer';
import TroublePlaceModel from '../../models/TroublePlaceModel';
import AddRequestToCopModal from '../modal/AddRequestToCop';
import { EVENTS } from '../../events/channels_events_name';

export default Marionette.View.extend({
    tagName: 'div',
    className: 'message-task',
    template: template,
    events: {
        'click .close-message-task': 'close_message_task',
        'click a.link': 'open_this_cop_detailed',
        'click .connect_us__toggle': 'show_request_to_cop_modal'
    },
    initialize: function (options) {
        //var policemen_arr = Server().get_items_by_name('policemans');
        //TODO: check parse_from... on most elegant path
        this.model = new TroublePlaceModel(this.model, {from: 'view'});
        options.model.policeman ?
        this.model.set('police_man',
            (new PolicemanModel(options.model.policeman, {parse_from_district:true})).toJSON()) : null;
    },
    close_message_task: function () {
        this.remove();
    },
    remove: function () {
        this.trigger(EVENTS.DETAILED_CLOSE);
        Marionette.View.prototype.remove.apply(this, arguments);
    },
    open_this_cop_detailed: function(){
        this.trigger(EVENTS.TROUBLE_PLACE_COP_DETAILED_SHOW, this.model.get('police_man'));
    },
    show_request_to_cop_modal : function(){
        var self = this;

        (new AddRequestToCopModal({'model' : this.model.get('police_man')})).render();
    }
});