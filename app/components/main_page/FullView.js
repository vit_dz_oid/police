/**
 * Created by DzEN on 9/12/2016.
 */
import Marionette from 'backbone.marionette';
import Backbone from  'backbone';
import log from 'loglevel';

import MapView from '../map/MapView';
import AddPointForm from '../add_request/AddPointWindow';
import SearchView from '../search_map_input/SearchView';
import template from './full_view.html';


export default Marionette.View.extend({
    template: template,
    className: 'main-page map-page body_map',
    addPointForm: null,
    mapMarker: null,
    regions: {
        map_view_region: '.wrapper.container-fluid',
        add_point_region: '.add-point-region',
        search_input: '.search-form form'
    },
    onRender: function () {
        var self = this;

        let regex = RegExp(/map\//);
        let path = Backbone.history.getFragment();
        let city = regex.test(path) ? path.replace("map/", "") : "";

        self.showChildView('map_view_region', new MapView({city: city}));
        self.showChildView('add_point_region', new AddPointForm());
        self.showChildView('search_input', new SearchView());

        self.$('.social-likes').socialLikes({
            zeroes: true,
            counters: true,
        });

        self.aligner(
            {
                aligned: self.$el.find('.ui'),// who must be aligned
                aligner: self.$el.find('.add_task'),
                listen_to: self.$el.find('.btn-plus'), // we must watch for this
                listen_to_event: ["attributes", "attributeOldValue"], //what events we must listen
                attr: 'margin-left'
            }
        );
    },
    aligner: function (data) {
        if (!data.aligned || !data.listen_to || !data.aligner || !data.listen_to_event || !data.attr) {
            log.info("Bad params");
            return;
        }

        var aligned = data.aligned;
        var listen_to = data.listen_to;
        var attr = data.attr;
        var aligner = data.aligner;
/*
        /!*Get window size*!/
        var window_height = $(window).height();
        var window_width = $(window).width();

        /!*Get aligned el size*!/
        var aligned_height = aligned.height();
        var aligned_width = aligned.width();

        /!*Get listen_to el size*!/
        var aligner_to_height = aligner.height();
        var aligner_to_width = aligner.width();*/

        // select the target node
        var target = listen_to[0];

        // create an observer instance
        var observer = new MutationObserver(function (mutations) {
            mutations.forEach(function (mutation) {
                var window_width = $(window).width();
                var aligner_to_width = aligner.width();
                var aligned_width = aligned.width();
                var total = 0;
                log.info("Mutation ", mutation);
                if (mutation.oldValue.split(' ').indexOf('open') < 0) {
                    total = window_width - aligned_width - aligner_to_width;
                } else {
                    total = window_width - aligned_width;
                }

                total /= 2;
                aligned.attr('style', attr + ' : ' + total + 'px;');
            });
        });

        // configuration of the observer:
        var config = {attributes: true, attributeOldValue: true};

        // pass in the target node, as well as the observer options
        observer.observe(target, config);

        // later, you can stop observing
        //observer.disconnect();
    }
});