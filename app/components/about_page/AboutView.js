/**
 * Created by DzEN on 10/7/2016.
 */
import Backbone from 'backbone';
import Marionette from 'backbone.marionette';

import $ from 'jquery';
import log from 'loglevel';

import template from './about.html';
import config from '../../config';


var need_validation_els = [
    {
        name : "name",
        pattern : /.{3,}/i
    },
    {
        name : "email",
        pattern : /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i
    },
    {
        name : "message",
        pattern : /.{10,80}/i
    }
]

export default Marionette.View.extend({
    template: template,
    className: 'main-page common-pages',
    events : {
        'click a.city': 'city_checked',
        'click a.add-own-city' : 'restore_form',
        'click .btn-add-city': 'add_city_from_check_city_modal',
        'click #submit-form-add-city': 'submit_add_city',
        'blur input[type!="file"]': 'validate_input',
        'blur textarea': 'validate_input',
        'click .wrap_a_s_p': 'stop_close_modal',
        'click .add-own-city-bottom': 'cause_add_own_city_click'
    },
    onRender: function(){
        var self = this;
        //Stupid hack
        var hash = window.location.hash.split('/');

        setTimeout(function(){
            if(hash.length > 1 ){
                self.scroll_to_anchor(hash[1]);
            } else {
                self.scroll_to_body_top();
            }
        }, 0);
    },
    stop_close_modal: function(e){
        e.stopPropagation();
        e.preventDefault();
    },
    restore_form: function(){
        var self = this;
        //Clear input field and remove red border
        _.each(self.$('#form-add-city input'), function (el) {
            $(el).val('').removeClass('error-input');
        });

        _.each(self.$('#form-add-city textarea'), function (el) {
            $(el).val('').removeClass('error-input');
        });

        //Hide error message
        _.each(self.$('#form-add-city .error-message'), function (item) {
            $(item).addClass('hidden');
        });
        self.toggle_form_add_city(true);
    },
    submit_add_city: function(e){
        var self = this;
        e.stopPropagation();
        e.preventDefault();
        if(!self.new_validation()) {
          return false;
        }
        var data = $(self.$('#form-add-city')).serializeArray();
        var dataObj = _.object(_.pluck(data, 'name'), _.pluck(data, 'value'));
      $.ajax({
        url: config.ROOT_API_URL + "city_proposals",
        type: 'POST',
        data: dataObj,
        success: function() {
            console.log('Request add city success');
        }
      });

      self.toggle_form_add_city(false);
    },
    toggle_form_add_city: function (val){
        var self = this;

        if(val){
            self.$el.find('.wrap_a_s_p').css('display', 'block');
            self.$el.find('.wrap_a_s_p_success').css('display', 'none');
        } else {
            self.$el.find('.wrap_a_s_p').css('display', 'none');
            self.$el.find('.wrap_a_s_p_success').css('display', 'block');
        }
    },
    add_city_from_check_city_modal: function(e){
        var self = this;
        e.stopPropagation();
        $('.add_sity_pop a.add-own-city').dropdown('toggle');
        $('#modal-ch-s').modal('hide');
        self.restore_form();
        self.toggle_form_add_city(true);
    },
    validate_input : function(e){
        var self = this;
        var value = e.target.value;
        var $el = $(e.target);
        var pattern = _.chain(need_validation_els).findWhere({name: $el.attr('name')}).value();

        log.info("Pattern ", pattern);

        if(pattern){
            pattern = pattern.pattern;
        } else {
            return;
        }

        if (!pattern.test(value)){
            $el.addClass('error-input');
            $el.find('+ .error-message').removeClass('hidden');
        } else {
            $el.removeClass('error-input');
            $el.find('+ .error-message').addClass('hidden');
        }
    },
    new_validation : function(){
        var self = this;
        var hasError = true;
        var form = self.$el.find('#form-add-city');
        var el = null;

        _.each(need_validation_els, function(item){
            el = form.find("[name='" + item.name + "']");

            if (!item.pattern.test(el.val())){
                el.addClass('error-input');
                el.find('+ .error-message').removeClass('hidden');
                //Toggle that have as min one error field
                hasError = false;
            } else {
                el.removeClass('error-input');
                el.find('+ .error-message').addClass('hidden');
            }
        });

        return hasError;
    },
    city_checked: function(e){
      var path = 'map/' + $(e.currentTarget).data('city');
      log.info("Navigate to ", path);

      setTimeout(function(){
        Backbone.history.navigate(path, {trigger: true});
      }, 250);
    },
    scroll_to_anchor : function(fuckingAnchor){
        this.scroll_to('#' + fuckingAnchor);
    },
    scroll_to_body_top : function(){
        this.scroll_to('body');
    },
    scroll_to: function(anchorSelector){
        $('html,body').animate({scrollTop: $(anchorSelector).offset().top},'slow')
    },
    cause_add_own_city_click: function () {
        $('.add-own-city').get(0).click();
        return false;
    }
});
