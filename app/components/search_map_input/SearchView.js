/**
 * Created by DzEN on 10/10/2016.
 */
import Radio from 'backbone.radio';
import $ from 'jquery';
import log from 'loglevel';

import Marionette from 'backbone.marionette';
import PolicemanModel from '../../models/PolicemanModel';
import template from './search.html';
import { CHANNELS, EVENTS } from '../../events/channels_events_name';

export default Marionette.View.extend({
    tagName: 'div',
    className: 'form-group',
    template: template,
    events: {
        'keypress #input-search': function(e) {
            return e.which !== 13;
        },
        'keyup #input-search': 'remove_marker'
    },
    ui : {
        input: "input"
    },
    initialize: function(){
        var self = this;

        log.info("Search view is initialize");

        self.channel = Radio.channel(CHANNELS.MAP_VIEW_ADD_POINT);
        ({gmap: self.gmap, google_maps : self.google_maps} = self.channel.request(EVENTS.MAP_GET));

        if(!!self.gmap){
            //Map ready at this moment
            self.map_flag_ready = true;
        } else {
            self.channel = Radio.channel(CHANNELS.MAP_VIEW);
            self.listenTo(self.channel, EVENTS.MAP_INITIALIZED, (param) => {
                ({gmap: self.gmap, google_maps : self.google_maps} = param);
                self.map_flag_ready = true;
                self.activate_input_find_place();
            });
        }
    },
    onRender: function(){
        var self = this;

        if(self.map_flag_ready){
            self.activate_input_find_place();
        }
    },
    activate_input_find_place: function () {
        var self = this;
        log.info("Self ", self);
        var input = self.getUI('input')[0];

        var map = self.gmap;

        // Create the autocomplete helper, and associate it with
        // an HTML text input box.
        var autocomplete = new self.google_maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        self.mapMarker = new self.google_maps.Marker({
            map: map,
        });

        // Get the full place details when the user selects a place from the
        // list of suggestions.

        self.map_listener = self.google_maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                return;
            }

            map.setCenter(place.geometry.location);
            map.setZoom(17);
            self.mapMarker.setOptions({url: place.url});


            // Set the position of the marker using the place ID and location.
            self.mapMarker.setPlace(/** @type {!self.google.Place} */ ({
                placeId: place.place_id,
                location: place.geometry.location
            }));
            self.mapMarker.setVisible(true);
        });

        //event not work from config :(
/*        self.$("#input-search").off('keyup');
        self.$("#input-search").on('keyup', function(e){
            self.remove_marker(e);
        });*/
    },
    remove_marker: function(e){
        var self = this;

        if(self.$(e.target).val().trim() === ''){
            self.mapMarker.setVisible(false);
        }
    }
});