/**
 * Created by DzEN on 11/24/2016.
 */
import Marionette from 'backbone.marionette';
import log from 'loglevel';

import template from './image_prev.html';

export default Marionette.View.extend({
    template: template,
    triggers : {
        'click .delete-image':'image:delete'
    },
    remove_this : function(){
        log.info("Try to remove ", this.model);
    }
});