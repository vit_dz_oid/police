/**
 * Created by DzEN on 11/24/2016.
 */
import Marionette from 'backbone.marionette';
import log from 'loglevel';

import ImagePreview from './ImagePreview';

export default Marionette.CollectionView.extend({
    id: 'result-images',
    className: 'clearfix',
    tegName: 'div',
    collection: new Backbone.Collection(),
    childView: ImagePreview,
    childViewEvents : {
        'image:delete' : 'delete_image'
    },
    delete_image: function(childView){
        log.info("ImagesPreview listen child : ", childView, childView.model.cid, this.collection.findWhere({cid : childView.model.cid}));
        let model = this.collection.findWhere({title : childView.model.get('title')});
        model ? this.collection.remove(model) : null;
    },
    delete_all_images : function(){
        log.info("ImagesPreview | delete_all_images | ");
        this.collection.reset();
    }
});