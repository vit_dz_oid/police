/**
 * Created by DzEN on 11/30/2016.
 */
import Marionette from 'backbone.marionette';
import Radio from 'backbone.radio';
import log from 'loglevel';

import template from './bodyModal.html';
import Toast from '../toast/Toast';
import { CHANNELS, EVENTS } from '../../events/channels_events_name';
import config from '../../config';

var need_validation_els = [
    {
        name : "text",
        pattern : /.{1,320}/i
    },
    {
        name : "name",
        pattern : /.{3,}/i
    },
    {
        name : "email",
        pattern : /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i

    }
];

export default Marionette.View.extend({
    className: 'modal-body',
    template: template,
    events : {
        'submit form': 'form_submit'
    },
    initialize : function(options){
        var self = this;
        log.info(arguments);
        self.close_parent = options.close_parent;
        self.channel = Radio.channel(CHANNELS.MAP_VIEW);
        self.city = self.channel.request(EVENTS.MAP_GET_CURRENT_CITY);
    },
    form_submit: function(e){
        var self = this;
        e.preventDefault();

        if (self.validation()) {
            /*
             "city_id" => "1",
             "policeman_id" => "1",
             "text" => "sdjhasdkjfh askjdfh aksljdhf",
             "name" => "Вася",
             "email" => "example@dsf.com"
             */

            var formdata = self.$('form').serializeArray();
            var data = {};
            self.disable_form();
            _.each(formdata, function(item){
                data[item.name] = item.value;
            });

            data.city_id = self.city.id;
            data.policeman_id = self.model.id;

            $.ajax({
                url: config.ROOT_API_URL + "questions",
                type: 'POST',
                data: data,
                success: function (data) {
                    new Toast({message : "Запит успішно відправлений"});
                },
                error: function(){
                    log.warn("Add point error : ", arguments);
                },
                complete: function(){
                    self.enable_form();
                    self.close_parent();
                },
                cache: false
            });
        }
    },
    validation : function(){
        var self = this;
        var hasError = true;
        var form = self.$el;
        var el = null;

        _.each(need_validation_els, function(item){
            el = form.find("[name='" + item.name + "']");

            if (!item.pattern.test(el.val())){
                el.addClass('error-input');
                el.find('+ .error-message').removeClass('hidden');
                //Toggle that have as min one error field
                hasError = false;
            } else {
                el.removeClass('error-input');
                el.find('+ .error-message').addClass('hidden');
            }
        });

        return hasError;
    },
    disable_form: function(){
        var self = this;
        self.form_toggle(false);
    },
    enable_form: function(){
        var self = this;
        self.form_toggle(true);
    },
    form_toggle: function(flag){
        var self = this;

        _.each(self.$('input'), function(el){
            flag ? $(el).removeAttr("disabled") : $(el).attr("disabled", "disabled");
        });

        _.each(self.$('textarea'), function(el){
            flag ? $(el).removeAttr("disabled") : $(el).attr("disabled", "disabled");
        });

        _.each(self.$('button'), function(el){
            flag ? $(el).removeAttr("disabled") : $(el).attr("disabled", "disabled");
        });
    },
});
