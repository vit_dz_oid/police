/**
 * Created by DzEN on 11/30/2016.
 */
import Marionette from 'backbone.marionette';
import template from './bodyModal.html';

export default Marionette.View.extend({
    className: 'empty',
    template: '<div></div>'
});