/**
 * Created by DzEN on 11/30/2016.
 */
import BackboneBootstrapModals from 'backbone-bootstrap-modals';
import BodyView from './BodyView';
import FooterView from './EmptyFooter';

export default BackboneBootstrapModals.BaseModal.extend({
    id: 'connect_us',
    className: 'modal request-cop',
    headerView: BackboneBootstrapModals.BaseHeaderView,
    bodyView: BodyView,
    bodyViewOptions: function() {
        var self = this;
        return {
            close_parent: function() {
                self.hide();
            }
        };
    },
    footerView : FooterView
});
