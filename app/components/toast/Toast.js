/**
 * Created by DzEN on 10/4/2016.
 */
import Marionette from 'backbone.marionette';
import $ from 'jquery';
import _ from 'underscore';
import PolicemanModel from '../../models/PolicemanModel';
import template from './toast.html';

export default Marionette.View.extend({
    className: 'success-message-block',
    template: template,
    initialize: function(message, options){
        var self = this;
        var own_options = _.extend({}, options || {});

        self.error = own_options.error || false;

        self.model = new Backbone.Model({message : message.message});

       setTimeout(()=>{
            self.remove();
        }, own_options.time || 3000);
        self.render();
    },
    onRender: function(){
        var self = this;
        if(self.error){
            self.$el.addClass("popover-error");
        }
        $('body').append(self.$el);
    }
})