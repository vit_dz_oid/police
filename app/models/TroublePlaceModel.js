/**
 * Created by DzEN on 9/14/2016.
 */
import Backbone from 'backbone';

export default Backbone.Model.extend({
    defaults: {
        place_id: 0,
        address: {
            text: "Вулиця Кутузова, 3",
            url: "#"
        },
        resolved: 0,//0 - no, 1- in process, 2 - yes
        request_message: {
            title: "Деребанять бюджет",
            message: "Депутат (у тєлєвізорі). А половина депутатів, шо сидять в сесійному залі, прийшли сюди " +
            "з тюрми, а ще по другій половині тюрма плаче… "
        },
        photo: [
            {
                url: "http://politica-ua.com/wp-content/uploads/2014/01/dep-sluga-pozvoni-23-01-2014.jpg"
            },
            {
                url: "http://upogau.org/netcat_files/1496_273.jpg"
            },
            {
                url: "https://focus.ua/modules/thumb.php?u=../files/images/5511/5511777.jpg"
            },
            {
                url: "https://focus.ua/modules/thumb.php?u=../files/images/5508/5508831.jpg"
            }
        ],
        response_message: {
            title: "Відповідь",
            message: ""
        },
        police_man: 1,
        success_icon: "assets/img/pin/pin_02.png",
        problem_icon: "assets/img/pin/pin_01.png",
        icon: "assets/img/pin/pin_01.png",
        type: 'trouble_place',
        position: {"lat": 0, "lng": 0}
    },
    initialize: function (attrs, options) {
        var self = this;

        self.set({icon: self.get_actual_icon(attrs.status === "processing" ? 0 : 1)});
        options && options.from ? null : self.parse_from_server(attrs);
    },
    parse_from_server: function(attr){
        /* {
         "id": 1,
         "full_address": "вулиця Антоновича, 6, Київ, місто Київ, Україна",
         "city": null,
         "subject": "Постоянно курва срут пид викном!",
         "message": "дорий день в мене пид викном кухни кажний день срут. еси бы сралы десь пид викном спальни чи коридра, а то кухня исты неможна вобще, зробить шось а тоце зрада !!! ",
         "response_message": null,
         "name": "каролина",
         "email": "karol@rambler.ru",
         "status": "processing",
         "lat": 50.4386953,
         "lng": 30.513313,
         "created_at": "2016-09-23T11:41:22.236Z",
         "updated_at": "2016-09-23T11:41:22.236Z"
         }*/
        var self = this;

        var data = {
            "address" : {
                text : attr.full_address,
                url: "#"
            },
            resolved: attr.status === "processing" ? 0 : 1,
            request_message: {
                title: attr.subject,
                message: attr.message
            },
            photo: attr.photos,
            response_message: {
                title: attr.response_message ? attr.response_message.title : "",
                message: attr.response_message ? attr.response_message : ""
            },
            police_man: 0,
            position: {
                lat: attr.lat,
                lng: attr.lng
            }
        };

        self.set(data);
    },
    get_actual_icon: function (resolved) {
        var self = this;

        switch (resolved) {
            case 1:
                return self.get('success_icon');
            case 0:
            default:
                return self.get('problem_icon');
        }
    }
})