/**
 * Created by DzEN on 9/23/2016.
 */
import Backbone from 'backbone';
import log from 'loglevel';

export default Backbone.Model.extend({
    default: {
        id: 0,
        coords: [],
        created_at: "2016-09-30T08:39:20.151Z",
        policemans: [
            {
                "avatar": "/system/policemans/avatars/000/000/003/original/walker.jpg?1475224761",
                "name": "Корделл Уокер",
                "address": "вул. Янгеля, 25",
                "phone": "+38(044) 777-77-77",
                "schedule": "Пн.-Чт. з 14:00 до 18:00"
            }
        ],
        updated_at: "2016-09-30T08:39:20.151Z",
        icon: "assets/img/pin/pin_03.png"
    },
    initialize: function(options){
        var self = this;

        if(!_.isArray(options.coords)){
            self.set("coords", JSON.parse( "[" + options.coords + "]"));
        }
    }
})
