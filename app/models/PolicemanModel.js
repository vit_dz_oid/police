/**
 * Created by DzEN on 9/14/2016.
 */
import Backbone from 'backbone';

export default Backbone.Model.extend({
    defaults: {
        policeman_id: 0,
        name: "Іван Фуражко",
        rank: "",
        position: {lat: 0.0000, lng: 0.0000},
        contact: {
            address: "Вул. Лютеранська 3",
            tel: "+38 (044) 777-77-77"
        },
        work_time: {
            days: "П'ятниця - Четвер",
            time: "З 10.00 до 12.00" // прикольна робота
        },
        icon: "assets/img/pin/pin_03.png",
        photo: "assets/img/front/cop.png",
        type: 'policeman',
        region: []
    },
    initialize: function(model, options){
        let self = this;

        if(options && options.must_parse){
            /*Cop from /api/districts
             {
             "avatar": "/system/policemans/avatars/000/000/001/original/maclein.jpg?1475502048",
             "name": "Джон Макклейн 1",
             "address": "вул. Лютеранська, 3",
             "phone": "+38(044) 777-77-77",
             "schedule": "Пн.-Чт. з 14:00 до 18:00",
             "lat": 30.438309,
             "lng": 50.566666
             }
            */
            //Additional operations for compatibility
            //TODO : repair position!!!
            let data = {
                contact: {
                    address: model.address,
                    tel: model.phone
                },
                work_time: {
                    days: model.schedule,
                    time: null
                },
                position: {
                    lat: parseFloat(model.lat),
                    lng: parseFloat(model.lng)
                },
                photo: model.avatar
            };

           data.region =  _.map(model.region, function(item){
               return {
                   lat: parseFloat(item.lat),
                   lng: parseFloat(item.lng)
               }
           });

            self.set(data);
        }
    }
})