/**
 * Created by DzEN on 9/14/2016.
 */
import Backbone from 'backbone';

export default Backbone.Model.extend({
    default: {
        position: {
            lat: 0,
            lng: 0,
        }
    }
})