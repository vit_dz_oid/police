/**
 * Created by DzEN on 9/14/2016.
 */

import Mn from 'backbone.marionette';
import Controller from 'controllers/RoutingController';
import log from 'loglevel';

export default Mn.AppRouter.extend({

    //Search in controllers
    appRoutes : {
        '': 'index_page',
        'index': 'index_page',
        'map(/:city)': 'main_page',
        'about(/:anchor)': 'about_page',
        'mobile': 'mobile_page'
    },

    initialize : function(options){
        var self = this;

        /*
         log.trace(msg)
         log.debug(msg)
         log.info(msg)
         log.warn(msg)
         log.error(msg)
         */
        log.setLevel('info');
        log.info('AppRouter is loaded');

        this.controller = new Controller({
            region: this.getOption('region')
        });
    },
    onRoute: function(name, path, args) {
      if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || screen.width <= 699) {
        if (path !== 'mobile') {
          this.navigate('mobile', {trigger: true, replace: true});
        }
      }
      log.info('User navigated to ' + path);
    }
});