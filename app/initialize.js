/**
 * Created by DzEN on 9/10/2016.
 */

import './assets/css/normalize.css';
import 'bootstrap/dist/css/bootstrap.css';
import './assets/css/style.css';
import $ from 'bootstrap/dist/js/bootstrap';
import App from './App';

document.addEventListener('DOMContentLoaded', () => {
    // consolelog("App loading");
    const app = new App();
    app.start();
});
